# myka

## CLI API

Four branches of functionality: Culture, Line, Harvest and Recipe.

**Culture** refers to a single unique culture; these cultures collectively constitute a culture library. 

**Line** refers to a single mycelium resulting from an expansion action.

**Harvest** refers to the yield obtained from a line.

**Recipe** refers to a single substrate recipe.

As an example:

We have a Culture: Ganoderma lucidum 'Aleon1' from FungiPerfecti.

We have a Line of that Culture: Myceliated-sawdust in a 1L jar, sterilised.

We have a Recipe for that Line: Woodlover's sawdust / wood chip mix.

We have a Harvest of that Line: First flush, 270 g fresh.

### Commands

Generic:

```
myka add
myka delete
myka list
```

Culture:

```
myka add culture <genus> <species> <strain> <source> <culture_id>
myka delete culture <culture_id>
myka list culture <culture_id>
myka list cultures
```

Line:

```
myka add line <parent_id> <recipe_id> <container>
myka delete line <line_id>
myka compost line <line_id>
myka list line <line_id>
myka list lines
```

Harvest:

```
myka add harvest <line_id> <fresh_mass>
myka list harvest <harvest_id>
myka list harvests
```

Recipe:

```
myka add recipe <recipe_id> <ingredients> <quantities> <treatment>
myka delete recipe
myka list recipe <recipe_id>
myka list recipes
```

Optional flags allow filtering of results.

_List of examples is non-exhaustive_

```
-g      // --genus
-s      // --species
-c      // --container
-i      // --ingredient
-r      // --recipe
```

List all harvests for Ganoderma genus:

```
myka list harvests -g Ganoderma
myka list harvests --genus Ganoderma
```

List all harvests for Hericium erinaceus species:

```
myka list harvests -g Hericium -s erinaceus
myka list harvests --genus Hericium --species erinaceus
```

List all lines in jars:

```
myka list lines -c jar
myka list lines --container jar
```

List all lines of Pleurotus in buckets:

```
myka list lines -g Pleurotus -c bucket
myka list lines --genus Pleurotus --container bucket
```

List all recipes with horse manure:

```
myka list recipes -i hpoo
myka list recipes --ingredient hpoo
```

List all petri dishes with malt extract agar:

```
myka list lines -c petri -r mea
myka list lines --container petri --recipe mea
```
