table! {
    cultures (id) {
        id -> Integer,
        genus -> Text,
        species -> Text,
        strain -> Text,
        source -> Text,
        culture_id -> Text,
    }
}
