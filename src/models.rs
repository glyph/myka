use super::schema::cultures;

// NewCulture struct allows for adding a new culture to the library
#[derive(Insertable)]
#[table_name = "cultures"]
pub struct NewCulture {
    pub genus: String,
    pub species: String,
    pub strain: String,
    pub source: String,
    pub culture_id: String,
}

impl NewCulture {
    // Constructor method for returning an instance of `NewCulture`
    pub fn new(
        genus: String,
        species: String,
        strain: String,
        source: String,
        culture_id: String,
    ) -> NewCulture {
        NewCulture {
            genus,
            species,
            strain,
            source,
            culture_id,
        }
    }
}

#[derive(Queryable)]
pub struct Culture {
    pub id: i32,
    pub genus: String,
    pub species: String,
    pub strain: String,
    pub source: String,
    pub culture_id: String,
}

/*
#[derive(Insertable)]
#[table_name = "lines"]
pub struct NewLine {
    pub culture_id: String,
    pub container: String,
    pub dimensions: String,
    pub substrate: String,
    pub treatment: String,
}

impl NewLine {
    pub fn new(culture_id: String, container: String, dimensions: String, substrate: String, treatment: String) -> NewLine {
        NewLine {
            culture_id,
            container,
            dimensions,
            substrate,
            treatment,
        }
    }
}

#[derive(Queryable)]
pub struct Line {
    pub id: i32,
    pub container: String,
    pub dimensions: String,
    pub substrate: String,
    pub treatment: String,
    pub parent_id: i32,
    pub compost: bool,
    pub contam: bool,
    pub culture_id: String,
    pub level: i32,
}
*/
