# myka

## Development Setup Notes

```
cargo install diesel_cli --no-default-features --features sqlite
source $HOME/.cargo/env
diesel migration generate create_cultures
diesel migration generate create_lines
```

Write `up.sql` and `down.sql` for both tables.

```
diesel migration run
```
