# myka

## Data Model

----------

```
Culture {
    id              // internal db id for record
    genus           // genus name for the culture
    species         // species name for the culture
    strain          // strain designation for the culture
    source          // source from which the culture was obtained
    culture_id      // unique id for this specific culture
}
```

----------

```
Line {
    id              // internal db id for record
    culture_id      // unique id of source culture
    parent_id       // unique id of line's parent
    container       // container type (e.g. petri, jar, bag)
    dimensions      // dimensions of container
    substrate       // substrate / media composition
    treatment       // treatment of substrate
    timestamp       // date and time of expansion
    compost         // indicates removal from active cultivation
    contam          // contamination status
    level           // indentation level (for display purposes)
}
```

----------

```
Harvest {
    id              // internal db id for record
    culture_id      // unique id of source culture
    line_id         // unique id of source line (from which harvest is made)
    timestamp       // date and time of harvest
    flush           // harvest index, relative to other harvests
    fresh_mass      // mass of freshly-harvested mushrooms / sclerotia
    dry_mass        // mass of dried mushrooms / sclerotia
}
```
