# myka

## Roadmap

**Culture**

 - get
   - [x] genus
   - [x] species
   - [x] strain
   - [x] cultures count (total)
 - set
   - [x] genus
   - [x] species
   - [x] strain
   - [x] source
   - [ ] container
   - [ ] substrate
   - [ ] recipe id
   - [ ] unique id
   - [ ] date
   - [ ] active
   - [ ] backups
   - [ ] lost (bool)*
 - del
   - [x] culture by id

`* flag for loss of culture from library`

**Line**

 - get
   - [ ] colony id
   - [ ] colony
   - [ ] active cultures
 - set

**Harvest**

 - get
 - set

**Recipe**

 - get
   - [ ] recipe id
   - [ ] ingredients
   - [ ] process
 - set
   - [ ] recipe id
   - [ ] ingredients
   - [ ] process

AGPL-3.0.
