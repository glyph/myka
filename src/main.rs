extern crate diesel;
extern crate myka;

use self::myka::*;
use anyhow::Result;
use structopt::StructOpt;

// Application
#[derive(Debug, StructOpt)]
struct Cli {
    #[structopt(subcommand)]
    culture: Culture,
}

#[derive(Debug, StructOpt)]
enum Culture {
    #[structopt(name = "add")]
    /// Add a culture to the Library
    Add {
        genus: String,      // Hericium
        species: String,    // erinaceus
        strain: String,     // UK
        source: String,     // WG
        culture_id: String, // HEUKWG001
    },

    #[structopt(name = "delete")]
    /// Delete a culture from the Library
    Delete { culture_id: String },

    #[structopt(name = "list")]
    /// List cultures from the Library
    List { culture_id: Option<String> },
}

fn main() -> Result<()> {
    let args = Cli::from_args();

    match args.culture {
        Culture::Add {
            genus,
            species,
            strain,
            source,
            culture_id,
        } => {
            add_culture(genus, species, strain, source, culture_id)?;
        }
        Culture::Delete { culture_id } => {
            delete_culture(culture_id)?;
        }
        Culture::List { culture_id: None } => {
            list_cultures()?;
        }
        Culture::List { culture_id } => {
            list_culture(culture_id)?;
        }
    }

    Ok(())
}
